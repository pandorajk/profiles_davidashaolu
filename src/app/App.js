import React from "react";
import "./App.css";
import Routes from "./components/routes";

const App = () => {
  return <Routes />;
};

export default App;
