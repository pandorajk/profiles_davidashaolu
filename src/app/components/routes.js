import React from "react";
import { Route, Switch } from "react-router-dom";
// REAL / TEST CONTENT:
import { RoutesLinks } from "./routes-links";
import { TestRoutesLinks } from "./test-route-links";

import Navbar from "../../components/navbar";
import About from "../../components/about";
import Contact from "../../components/contact";
import Portfolio from "../../components/portfolio";
import CoolCodingShowcase from "../../components/portfolio/portfolio-items/cool-coding-2020-view";
import ClubCode from "../../components/portfolio/portfolio-items/club-code-view"
import CodingSummerHome from "../../components/coding-summer-course/";
import CodingSummerCourse from "../../components/coding-summer-course/coding-summer-course-view";
import CodingSummerCourseFinished from "../../components/coding-summer-course/components/course-finished-view";
import SessionView from "../../components/coding-summer-course/coding-summer-session-view";
import Footer from "../../components/footer";

const Routes = () => {
  return (
    <>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <About />
        </Route>
        <Route exact path="/contact">
          <Contact />
        </Route>
        <Route exact path="/showcase">
          <Portfolio />
        </Route>
        <Route exact path="/portfolio">
          <Portfolio />
        </Route>
        <Route exact path="/portfolio/cool-coding-2020">
          <CoolCodingShowcase />
        </Route>
        <Route exact path="/portfolio/club-code">
          <ClubCode />
        </Route>
        <Route exact path={`/${RoutesLinks.CODINGSUMMER}`}>
          <CodingSummerCourseFinished />
        </Route>
        <Route exact path={`/${RoutesLinks.CODINGSUMMER}/age_8-10`}>
          <CodingSummerCourseFinished />
        </Route>
        <Route exact path={`/${RoutesLinks.CODINGSUMMER}/age_11-14`}>
          <CodingSummerCourseFinished />
        </Route>
        <Route exact path={`/${RoutesLinks.CODINGSUMMER}/age_8-10/:session`}>
          <CodingSummerCourseFinished />
        </Route>
        <Route exact path={`/${RoutesLinks.CODINGSUMMER}/age_11-14/:session`}>
          <CodingSummerCourseFinished />
        </Route>

        {/* TEST CONTENT BELOW */}
        <Route exact path={`/${TestRoutesLinks.CODINGSUMMER}`}>
          <CodingSummerHome />
        </Route>
        <Route exact path={`/${TestRoutesLinks.CODINGSUMMER}/age_8-10`}>
          <CodingSummerCourse />
        </Route>
        <Route exact path={`/${TestRoutesLinks.CODINGSUMMER}/age_11-14`}>
          <CodingSummerCourse />
        </Route>
        <Route
          exact
          path={`/${TestRoutesLinks.CODINGSUMMER}/age_8-10/:session`}
        >
          <SessionView />
        </Route>
        <Route
          exact
          path={`/${TestRoutesLinks.CODINGSUMMER}/age_11-14/:session`}
        >
          <SessionView />
        </Route>
      </Switch>
      <Footer />
    </>
  );
};

export default Routes;
