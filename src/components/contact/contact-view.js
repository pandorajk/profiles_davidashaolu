import React, { useState } from "react";
import { Form, Button, Card, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ContactMe = () => {
  const [formContent, setFormContent] = useState({
    name: "",
    school: "",
    body: "",
  });
  const [displayMessage, setDisplayMessage] = useState(false);

  const handleChange = (event) => {
    setFormContent({ ...formContent, [event.target.name]: event.target.value });
  };

  const handleGenerateMessage = () => {
    setDisplayMessage(true);
  };

  return (
    <div className="splash-container-bottom all-content-center m-3">
      {/* <iframe
        src="https://docs.google.com/forms/d/e/1FAIpQLSfl7YULBWRELxyGRjfLSGOEFMcavykz6KkcvOj1nQZEgCf39Q/viewform?embedded=true"
        width="640"
        height="657"
        frameborder="0"
        marginheight="0"
        marginwidth="0"
        title="general contact"
      /> */}
      {/* <Button
        href="https://forms.gle/mvDnvsbmoScWAdgE9"
        target="blank"
        className="btn btn-primary"
      >
        Send in your work
      </Button> */}
      <div className={displayMessage ? "d-none" : ""}>
        <Form>
          <Form.Group as={Row} controlId="name">
            <Form.Label column sm="2" className="mr-4">
              Name
            </Form.Label>
            <Col sm="9">
              <Form.Control
                name="name"
                placeholder="Your Name"
                onChange={handleChange}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="school">
            <Form.Label column sm="2" className="mr-4">
              (School)
            </Form.Label>
            <Col sm="9">
              <Form.Control
                name="school"
                placeholder="Optional"
                onChange={handleChange}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="body">
            <Form.Label column sm="2" className="mr-4">
              Message
            </Form.Label>
            <Col sm="9">
              <Form.Control
                name="body"
                placeholder="Your message"
                onChange={handleChange}
                as="textarea"
                rows="6"
              />
            </Col>
          </Form.Group>
          <Button
            className="session-button ge-blue-colour-bg no-border"
            onClick={handleGenerateMessage}
          >
            Generate Message
          </Button>
        </Form>
      </div>
      <div className={displayMessage ? "" : "d-none"}>
        <Card className="p-5 card-align-left">
          <p>
            <span className="font-weight-bold">To:</span>
            {`  david.ashaolu@agorainternationalandorra.com`}
          </p>
          <p>
            <span className="font-weight-bold">From:</span>
            {formContent.name}
          </p>
          <p>
            <span className="font-weight-bold">School:</span>
            {formContent.school}
          </p>
          <p>
            <span className="font-weight-bold">Message:</span>
            {formContent.body}
          </p>
        </Card>
        <span>
          <Button
            className="session-button ge-blue-colour-bg text-white no-border"
            onClick={() => window.location.reload()}
          >
            Reset
          </Button>
        </span>
        <div className="session-button ge-blue-colour-bg text-white no-border">
          <a
            href={`mailto:david.ashaolu@agorainternationalandorra.com?subject=davidAshaolu - Contact Me&body=${`From: ${formContent.name} // School : ${formContent.school} //  Message : ${formContent.body}`}`}
          >
            Send
            <FontAwesomeIcon icon="envelope" className="ml-3 text-white" />
          </a>
        </div>
      </div>
      <span className="mt-3 mb-3">
        <a
          href="https://www.linkedin.com/in/david-ashaolu/"
          target="blank"
          alt="linked in link"
        >
          Find me on LinkedIn
          <FontAwesomeIcon icon="external-link-square-alt" />
        </a>
      </span>
    </div>
  );
};

export default ContactMe;
