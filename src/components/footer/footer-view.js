import React from "react";
// import { Link } from "react-router-dom";
// import { RoutesLinks } from "../../app/components/routes-links";
import "./footer-styles.css";

const Footer = () => {
  return (
    <div className="footer">
      <a
        href="https://gitlab.com/pandorajk"
        target="_blank"
        rel="noopener noreferrer"
      >
        Website created by Pandorajk | Built with React | Code on GitLab | 2020
      </a>
      {/* <Link to={RoutesLinks.CODINGSUMMER}>{">"}</Link> */}
    </div>
  );
};

export default Footer;
