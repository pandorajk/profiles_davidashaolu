import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Col, Card } from "react-bootstrap";
import CVPhoto from "../../images/cv_photo_david.png";
import StudentsCodingImage from "../../images/students-coding.png";
import ClubCodeImage from "../../images/club-code.png"
import SummerCoding2020Image from "../../images/agora-summer-2020.png"
import CoolCodingImage from "../../images/cool-coding-2020.png"
import "./about-styles.css";

const About = () => {
  return (
    <>
      <img
        src={StudentsCodingImage}
        className="background-home-img"
        alt="students coding together"
      />
      <div className="splash-container-home-top all-content-center text-white">
        {`< CODE A BETTER FUTURE />`}
      </div>
      <div>
        <Row className="pt-3 pb-3">
          <Col md="4" className="all-content-center p-5">
            <span className="mb-4">
              <span className="blue-colour h2 font-weight-bold">{`{ `}</span>
              <span className="red-colour h2 font-weight-bold">{`} `}</span>
              <span className="dblue-colour h2 font-weight-bold">
                davidAshaolu
              </span>
            </span>
            <p className="dblue-colour font-weight-bold">
              Qualified Teacher & Whole School Computer Science Coordinator
            </p>
            <p className="dblue-colour">
              at Agora International School, Andorra
            </p>
          </Col>
          <Col md="4" className="all-content-center">
            <img src={CVPhoto} className="cv-photo" alt="cv-headshot" />
          </Col>
          <Col md="4" className="all-content-center p-5">
            <p className="dblue-colour">
              I help children and young adults to become critical thinkers and
              digitally literate in the rapid changing world of technology
              through the implementation of computer science and computing in
              schools.
            </p>
          </Col>
        </Row>
        <Row className="red-colour-bg p-4">
          <Col>
            <Row className="mb-4">
              <Col className="all-content-center m-4 text-white font-weight-bold h1">
                {`{ What I'm about }`}
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Card className="p-4 all-content-center dgrey-colour-bg text-white about-card">
                  <p className="font-weight-bold h3">
                    <FontAwesomeIcon
                      icon="user-graduate"
                      className="h1 text-white"
                    />
                  </p>
                  <p>Students learn best when they are having fun.</p>
                  My pedagogy is for all children to learn through a engaging
                  playful way with the use of emerging online educational
                  platforms. In addition, technology can address the needs of
                  every type of learner to exceed expected attainment and
                  becoming the critical thinkers of tomorrow.
                </Card>
              </Col>
              <Col md="4">
                <Card className="p-4 m-4 all-content-center dgrey-colour-bg text-white about-card">
                  <p className="font-weight-bold h3">
                    <FontAwesomeIcon
                      icon="chalkboard-teacher"
                      className="h1 text-white"
                    />
                  </p>
                  <p>I am passionate about raising the profile of Computing.</p>
                  With 10 years of teaching and leadership roles, in the UK and
                  Internationally, I am engaged with shaping the direction and
                  development of the computer science curriculum in schools.
                </Card>
              </Col>
              <Col md="4">
                <Card className="p-4 all-content-center dgrey-colour-bg text-white about-card">
                  <p className="font-weight-bold h3">
                    <FontAwesomeIcon
                      icon="project-diagram"
                      className="h1 text-white"
                    />
                  </p>
                  <p>Working togther to support learning.</p>I value and
                  encourage parents, schools and community involvement in each
                  child's learning and facilitate opportunities for all
                  stakeholders to learn together, through initiatives such as
                  the Hour of Code. I keep up to date with the computer science
                  teaching community, attend conferences and explore online for
                  the latest activites.
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col className="p-5 m-4">
            <p className="dblue-colour font-weight-bold h3 mb-4">
              What is Computing and why is it so important?
            </p>
            <p>
              Computing, Digital, Tech, IT, Computer Science – whatever you like
              to call it, it’s everywhere!
            </p>
            <p>
              Computing opens up new ways of thinking - and is a powerful tool
              to help solve problems. It is helping to tackle the world’s
              biggest issues, like climate change and health, improving and
              saving lives around the World. Nearly every job needs some aspect
              of Computing.That's why embedding industry-level technologies into
              the curriculum is vital in expanding students horizons.
              computingatschool.org.uk/
            </p>
          </Col>
        </Row>
        <Row className="red-colour-bg p-4">
          <Col className="all-content-center m-4">
            <h3 className="m-4 text-white font-weight-bold h2">
              Check out my Portfolio...
            </h3>
            {/* <Button className="btn btn-dark"> */}
              <Link to="/portfolio" className="text-white font-weight-bold">
              <img src={ClubCodeImage} style={{height: '20vh', margin: '5px'}} alt="portfolio1" />
              <img src={SummerCoding2020Image} style={{height: '20vh', margin: '5px'}} alt="portfolio2" />
              <img src={CoolCodingImage} style={{height: '20vh', margin: '5px', backgroundColor: 'white'}} alt="portfolio3" />
              <p>
                <FontAwesomeIcon
                      icon="hand-point-right"
                      className="text-white"
                      style={{fontSize: '4em'}}
                  />
              </p>
              </Link>
            {/* </Button> */}
          </Col>
        </Row>
      </div>
    </>
  );
};

export default About;
