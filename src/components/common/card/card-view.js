import React from "react";
import { Card } from "react-bootstrap";

const CardView = ({ image, title, age, line2, link, linkNewTab }) => {
  const line1 = age ? `${title}, ${age}` : title;
  return (
    <Card className="card-container">
      <a href={link} target={linkNewTab} rel="noopener noreferrer">
        <img src={image} className="card-image" alt={image} />
        <div className="card-text">
          <div className="text-left">
            <b>{line1}</b>
          </div>
          <div className="text-left">
            <i>{line2}</i>
          </div>
        </div>
      </a>
    </Card>
  );
};

export default CardView;
