import React from "react";
import { Row, Col } from "react-bootstrap";
import CardView from "../common/card";
import SHOWCASE from "./showcase-cards-data";

const Portfolio = () => {
  return (
    <div className="top-margin-under-nav all-content-center ">
      <Row>
        <Col>
          <h3>My Portfolio</h3>
        </Col>
      </Row>
      <Row>
        <Col md={{ span: 10, offset: 1 }}>
          {SHOWCASE.map((showcase) => (
            <CardView
              title={showcase.title}
              line2={showcase.description}
              link={showcase.link}
              image={showcase.image}
              linkNewTab={showcase.target}
            />
          ))}
        </Col>
      </Row>
    </div>
  );
};

export default Portfolio;
