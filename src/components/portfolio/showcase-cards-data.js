import BlogCC2020 from "../../images/blog.png";
import CoolCoding2020 from "../../images/cool-coding-2020.png";
import AgoraSummer2020 from "../../images/agora-summer-2020.png";
import HoC2019 from "../../images/agora-hour-code-2019.png";
import HoC2020 from "../../images/agora-hour-code-2020.png";
import SaferInternet2020 from "../../images/agora-safer-internet-2020.png";
import Teaching2020 from "../../images/agora-teaching-2020.png";
import Sensational from "../../images/sensational.png";
import ClubCode from "../../images/club-code.png"

const SHOWCASE = [
  {
    title: "Hour of Code 2020",
    description: "Agora International School, Andorra",
    link: "https://www.instagram.com/p/CInOj5DH54Z/",
    image: HoC2020,
    target: "blank",
  },
  {
    title: "Club Code",
    description: "After School Coding Club at Agora",
    link: "portfolio/club-code",
    image: ClubCode,
    target: "",
  },
  {
    title: "Globeducate Blog Summer 2020",
    description: "Cool Coding 2020",
    link:
      "https://www.globeducate.com/code-a-better-future-globeducate-cool-coders/",
    image: BlogCC2020,
    target: "blank",
  },
  {
    title: "* Cool Coding 2020 SHOWCASE *",
    description: "Student showcase",
    link: "portfolio/cool-coding-2020",
    image: CoolCoding2020,
    target: "",
  },
  {
    title: "Summer Camp 2020",
    description: "Agora International School, Andorra",
    link: "https://www.instagram.com/p/CC8Sa2AneML/",
    image: AgoraSummer2020,
    target: "blank",
  },
  {
    title: "Hour of Code 2019",
    description: "Agora International School, Andorra",
    link: "https://www.instagram.com/p/B53eNiWn8k1/",
    image: HoC2019,
    target: "blank",
  },
  {
    title: "Safer Internet Day 2020",
    description: "Agora International School, Andorra",
    link: "https://www.instagram.com/p/B8Rx_9rnc0S/",
    image: SaferInternet2020,
    target: "blank",
  },
  {
    title: "1st year at Agora, 2019-2020",
    description: "Agora International School, Andorra",
    link: "https://www.instagram.com/p/B5-2TEXnCkJ/",
    image: Teaching2020,
    target: "blank",
  },
  {
    title: "The Benefit of Exercise for Cognitive Development",
    description: "SENsational Tutors",
    link:
      "https://www.sensationaltutors.co.uk/the-effect-of-exercise-on-cognitive-development-how-to-help-your-child-improve-their-learning/",
    image: Sensational,
    target: "blank",
  },
];

export default SHOWCASE;
