import React from "react";
import { Row, Col } from "react-bootstrap";
import ClubCodeImg from "../../../images/club-code.png";

const ClubCode = () => {
  return (
    <div className="top-margin-under-nav">
      <Row>
        <Col md={{span : 10, offset: 1}} style={{textAlign: 'center'}}>
          <img src={ClubCodeImg} style={{height: '70vh', margin: '5vh'}} alt="club code" />
        </Col>
      </Row>
    </div>
  );
};

export default ClubCode;
