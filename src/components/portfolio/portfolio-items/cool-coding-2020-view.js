import React from "react";
import { Row, Col } from "react-bootstrap";
import CardView from "../../common/card";
import STUDENTS from "../cool-coding-2020-showcase-data";
import GlobeducateLogo from "../../../images/globeducate-logo.png";

const CoolCoding2020 = () => {
  return (
    <div className="top-margin-under-nav">
      <Row>
        <Col>
          <img
            src={GlobeducateLogo}
            className="globeducate-logo"
            alt="Globeducate logo"
          />
          <h1 className="welcome-title dblue-colour">
            COOL CODING - SUMMER 2020!
          </h1>
          <div className="m-5">
            <p className="text-center">
              Check out some of the amazing games create by the students who
              particiapted in Cool Coding 2020. Each link is clickable and will
              take you to the site where you can play the games!
            </p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col className="text-center">
          {STUDENTS.map((student) => (
            <CardView
              title={student.name}
              line2={student.school}
              age={student.age}
              link={student.link}
              linkNewTab={"blank"}
              image={student.image}
            />
          ))}
        </Col>
      </Row>
    </div>
  );
};

export default CoolCoding2020;
