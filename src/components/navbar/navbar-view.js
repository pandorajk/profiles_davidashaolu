import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./navbar-styles.css";

const Navbar = () => {
  return (
    <div className="navbar dblue-colour-bg text-nowrap">
      <NavLink to={"/"} className="nav-text text-white">
        <FontAwesomeIcon icon="home" />
        {`  Home`}
      </NavLink>
      <span className="nav-text text-white">|</span>
      <NavLink to={"/"} className="nav-text text-white">
        {`{} davidAshaolu`}
      </NavLink>
      <span className="nav-text text-white">|</span>
      <NavLink to={"/contact"} className="nav-text text-white">
        Contact Me!
        <FontAwesomeIcon icon="envelope" />
      </NavLink>
    </div>
  );
};

export default Navbar;
