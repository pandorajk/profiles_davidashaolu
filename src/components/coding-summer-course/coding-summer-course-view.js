import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Row, Col, Alert } from "react-bootstrap";
// REAL / TEST CONTENT:
import { RoutesLinks } from "../../app/components/routes-links";
// import { TestRoutesLinks } from "../../app/components/test-route-links";
import CodingSummerNav from "./components/coding-summer-nav";
// REAL / TEST CONTENT:
import SummerData from "./summer-course-data";
// import TestData from "./test-course-data";
import CodingBackground from "../../images/motherboard.jpg";
import WelcomeAvatar from "../../images/welcome-avatar.png";
import ThumbsUp from "../../images/thumbs-up.png";
import "./coding-summer-styles.css";

const CodingSummerCourse = () => {
  const history = useHistory();
  const age_group = history.location.pathname.split("/")[2];

  // REAL / TEST CONTENT:
  const { INTRO } = SummerData;
  // const { INTRO } = TestData;

  // REAL CONTENT:
  const sessionButtons = () => {
    return Object.keys(RoutesLinks).map((link) => {
      if (link !== "CODINGSUMMER" && link.length === 8) {
        return (
          <Link
            to={`/${RoutesLinks.CODINGSUMMER}/${age_group}/${RoutesLinks[link]}`}
            className="session-button ge-blue-colour-bg"
            key={link}
          >
            {`Session ${link.slice(-1)}`}
          </Link>
        );
      } else if (link !== "CODINGSUMMER") {
        return (
          <Link
            to={`/${RoutesLinks.CODINGSUMMER}/${age_group}/${RoutesLinks[link]}`}
            className="session-button ge-blue-colour-bg"
            key={link}
          >
            {`Session ${link.slice(-2)}`}
          </Link>
        );
      } else {
        return null;
      }
    });
  };

  // // TEST CONTENT:
  // const testSessionButtons = () => {
  //   return Object.keys(TestRoutesLinks).map((link) => {
  //     if (link !== "CODINGSUMMER" && link.length === 8) {
  //       return (
  //         <Link
  //           to={`/${TestRoutesLinks.CODINGSUMMER}/${age_group}/${TestRoutesLinks[link]}`}
  //           className="session-button ge-blue-colour-bg"
  //           key={link}
  //         >
  //           {`Session ${link.slice(-1)}`}
  //         </Link>
  //       );
  //     } else if (link !== "CODINGSUMMER") {
  //       return (
  //         <Link
  //           to={`/${TestRoutesLinks.CODINGSUMMER}/${age_group}/${TestRoutesLinks[link]}`}
  //           className="session-button ge-blue-colour-bg"
  //           key={link}
  //         >
  //           {`Session ${link.slice(-2)}`}
  //         </Link>
  //       );
  //     } else {
  //       return null;
  //     }
  //   });
  // };

  return (
    <>
      <CodingSummerNav age_group={age_group} home={true} />
      <div className="coding-background">
        <img
          src={CodingBackground}
          className="coding-background-img"
          alt="coding background"
        />
      </div>
      <div className="header-box">
        <Row>
          <Col lg={{ span: 8, offset: 2 }}>
            <a href="https://fontmeme.com/pixel-fonts/">
              <img
                src="https://fontmeme.com/permalink/200610/f0fdeb189dc71e8802d834b343dd947c.png"
                alt="pixel-fonts"
                border="0"
                className="welcome-title-img"
              />
            </a>
          </Col>
        </Row>
      </div>
      <Row>
        <Col>
          <Alert variant="warning" className="text-center">
            This course end on Saturday 9th August 2020.
          </Alert>
        </Col>
      </Row>
      <Row className="m-4">
        <Col lg={{ span: 8, offset: 2 }}>
          <Row>
            <Col md="6" className="text-center">
              <img
                src={WelcomeAvatar}
                className="welcome-avatar"
                alt="welcome banner message"
              />
            </Col>
            <Col md="6" className="text-center">
              <p className="course-view-subtitle red-colour">
                Let's get started!
              </p>
              {INTRO.task}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="m-4">
        <Col lg={{ span: 8, offset: 2 }}>
          <Row>
            <Col md="6" className="text-center">
              <p className="course-view-subtitle ge-blue-colour">
                Your mission!
              </p>
              <p className="mb-3">{INTRO.mission}</p>
            </Col>
            <Col md="6" className="text-center">
              <iframe
                title="intro to sessions"
                // width="500"
                // height="300"
                src={`${INTRO.youtube}`}
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen={true}
                className="welcome-youtube"
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="m-4">
        <Col lg={{ span: 8, offset: 2 }}>
          <Row>
            <Col md="6" className="text-center">
              <img src={ThumbsUp} className="welcome-avatar" alt="thumbs up" />
            </Col>
            <Col md="6">
              <p className="course-view-subtitle ge-green-colour text-center">
                Please remember!
              </p>
              <ol>
                {INTRO.remember.map((note) => (
                  <li className="mb-2" key={note}>
                    {note}
                  </li>
                ))}
              </ol>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="m-5">
        <Col lg={{ span: 8, offset: 2 }} className="text-center">
          {/* REAL / TEST CONTENT: */}
          <div>{sessionButtons()}</div>
          {/* <div>{testSessionButtons()}</div> */}
        </Col>
      </Row>
      <Row className="m-4">
        <Col lg={{ span: 8, offset: 2 }} className="text-center">
          <p> With thanks to:</p>
          {INTRO[age_group].map((logo) => {
            return (
              <img src={logo} alt="brand logos" className="logos" key={logo} />
            );
          })}
        </Col>
      </Row>
      <Row className="m-4">
        <Col lg={{ span: 8, offset: 2 }}>
          <span className="red-colour font-weight-bold">IMPORTANT:</span> The
          contents of this course is only for students of Globeducate during the
          "Cool Coding - Video Game Creation" summer course (July - August
          2020). This is not for commercial use. This course has been created
          with no financial gain. Please do NOT share the URLs of this page with
          anyone else. All content and URLs will be removed at the end of the
          course.
        </Col>
      </Row>
    </>
  );
};

export default CodingSummerCourse;
