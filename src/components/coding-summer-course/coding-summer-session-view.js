import React, { useState } from "react";
import { useHistory, useParams, Link } from "react-router-dom";
import { Row, Col, Card, Button, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// REAL / TEST CONTENT:
import CourseContent from "./summer-course-content";
// import TestCourseContent from "./test-course-content";
import CodingSummerNav from "./components/coding-summer-nav";
import TooEarly from "./components/too-early-view";
import CreaterUsername from "./components/create-username";
import CSClassroom from "./components/cs-classroom";
import SendFeedback from "./components/send-feedback";
import One from "../../images/number-one.png";
import Two from "../../images/number-two.png";
import Three from "../../images/number-three.png";
import Four from "../../images/number-four.png";
import Five from "../../images/number-five.png";
import Finish1 from "../../images/finish1.jpeg";
import Finish2 from "../../images/finish2.jpeg";

import "./coding-summer-styles.css";

const Session = () => {
  const history = useHistory();
  const age_group = history.location.pathname.split("/")[2];
  const [show, setShow] = useState(true);

  const TODAYSESSION = 10;

  const params = useParams();
  // REAL / TEST CONTENT:
  const session = CourseContent[age_group][params.session];
  // const session = TestCourseContent[age_group][params.session];

  const instructionContent = (instructions) => {
    const activityDetails = (instruction) => {
      return instruction.map((item) => {
        const href = item.link ? (
          <div className="text-center">
            <Button className="session-button ge-green-colour-bg text-white no-border">
              <a href={item.link} target="blank">
                {item.line}
                <FontAwesomeIcon icon="external-link-alt" className="ml-2" />
              </a>
            </Button>
          </div>
        ) : null;
        if (item.type === "createUsername") {
          return (
            <div className="text-center">
              <CreaterUsername age_group={age_group} />
            </div>
          );
        }
        if (item.type === "send-username") {
          return (
            <iframe
              src="https://docs.google.com/forms/d/e/1FAIpQLSenAcF6ofCGDIiOLzsTALzVKpDFyAcN9Y5RTCl5iyhwZbnmTg/viewform?embedded=true"
              // width="640"
              height="613"
              frameBorder="0"
              marginHeight="0"
              marginWidth="0"
              title="send-username"
            />
          );
        }
        if (item.type === "selectCodeSparkClassroom") {
          return <CSClassroom />;
        }
        if (item.type === "send-feedback") {
          return <SendFeedback />;
        }
        if (item.logo) {
          return (
            <div className="text-center">
              <img src={item.logo} alt={item.alt} className="logos" />
            </div>
          );
        } else {
          return (
            <div
              className={`m-2 ${
                item.type === "title"
                  ? "font-weight-bold text-center h3"
                  : item.type === "important"
                  ? "font-weight-bold"
                  : item.type === "code"
                  ? "font-weight-bold text-center h2"
                  : null
              }`}
            >
              {href ? href : item.line}
            </div>
          );
        }
      });
    };
    return instructions.map((activity) => {
      return (
        <Card className="mb-3 p-3 session-card">
          {activityDetails(activity)}
        </Card>
      );
    });
  };

  if (!session.active) {
    return (
      <>
        <CodingSummerNav age_group={age_group} />
        <TooEarly />
      </>
    );
  }

  return (
    <>
      <CodingSummerNav age_group={age_group} />
      <Row className="session-margin-top text-center">
        <Col lg={{ span: 8, offset: 2 }}>
          <Row className="mt-3">
            <Col className="text-center">
              <p className="session-mission dblue-colour">{`Session ${session.number}`}</p>
              <p className="session-mission font-weight-bold">
                {session.mission}
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col lg={{ span: 8, offset: 2 }}>
          <Card className="m-3 p-3 ge-red-colour-bd">
            <Row className="mt-3">
              <Col className="col-2 ml-2">
                <img
                  src={One}
                  alt="instruction number"
                  className="instruction-number float-right"
                />
              </Col>
              <Col className="col-8 ml-2 mr-2">
                <span className="red-colour font-weight-bold">
                  You will need:
                </span>
                <ol>
                  {session.resources.map((resource) => (
                    <li
                      key={resource}
                      className={
                        resource.includes("support") ? "red-colour" : ""
                      }
                    >
                      {resource}
                    </li>
                  ))}
                </ol>
              </Col>
            </Row>
          </Card>
          <Card className="m-3 p-3 ge-yellow-colour-bd">
            <Row className="mt-3">
              <Col className="col-2 ml-2">
                <img
                  src={Two}
                  alt="instruction number"
                  className="instruction-number float-right"
                />
              </Col>
              <Col className="col-8 ml-2 mr-2">
                <div className="font-weight-bold">Instruction video:</div>
                <div className="text-center m-2">
                  <iframe
                    title={`session ${session.number} video`}
                    src={session.youtube}
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen={true}
                    disable={"true"}
                    className="session-youtube"
                  />
                </div>
              </Col>
            </Row>
          </Card>
          <Card className="m-3 p-3 ge-blue-colour-bd">
            <Row className="mt-3">
              <Col className="col-2 ml-2">
                <img
                  src={Three}
                  alt="instruction number"
                  className="instruction-number float-right"
                />
              </Col>
              <Col className="col-8 ml-2 mr-2">
                <p className="font-weight-bold">
                  {session.number < TODAYSESSION
                    ? "Recorded webinar link:"
                    : "Live webinar link:"}
                </p>
                <div className="text-center m-2"></div>
                {session.number >= TODAYSESSION ? (
                  <div className="text-center">{`at ${
                    age_group === "age_8-10"
                      ? "10:45H - 11:45H (CEST)"
                      : "12:00H - 13:00H (CEST)"
                  }`}</div>
                ) : null}
                <div className="all-content-center">
                  <a
                    href={session.webinar}
                    target={session.webinar ? "blank" : ""}
                    className="session-button ge-blue-colour-bg"
                  >
                    {session.number < TODAYSESSION ? "Webinar" : "Live Webinar"}
                    <FontAwesomeIcon
                      icon="external-link-alt"
                      className="ml-2"
                    />
                  </a>
                </div>
              </Col>
            </Row>
          </Card>
          <Card className="m-3 p-3 ge-green-colour-bd">
            <Row className="mt-3">
              <Col className="col-2 ml-2">
                <img
                  src={Four}
                  alt="instruction number"
                  className="instruction-number float-right"
                />
              </Col>
              <Col className="col-8 ml-2 mr-2">
                {instructionContent(session.instructions)}
              </Col>
            </Row>
          </Card>
          <Card className="m-3 p-3 ge-dblue-colour-bd">
            <Row className="mt-3">
              <Col className="col-2 ml-2">
                <img
                  src={Five}
                  alt="instruction number"
                  className="instruction-number float-right"
                />
              </Col>
              <Col className="col-8 ml-2 mr-2">
                <div className="font-weight-bold dblue-colour">
                  Well done - Mission Complete!
                </div>
                {session.number < 10 ? (
                  <>
                    <p>
                      Dont' forget to take screenshots of your work and send it
                      to me!
                    </p>
                    <Button className="session-button dblue-colour-bg text-white no-border">
                      <a
                        href={`https://forms.gle/LfYvz4hPDGwNB9rz6`}
                        target="blank"
                      >
                        Send
                        <FontAwesomeIcon
                          icon="external-link-alt"
                          className="ml-2"
                        />
                      </a>
                    </Button>
                  </>
                ) : (
                  <div className="all-content-center">
                    <Link
                      to="/showcase"
                      className="session-button ge-blue-colour-bg"
                    >
                      {age_group === "age_8-10" ? (
                        <img
                          src={Finish1}
                          className=""
                          alt="Finishing trophy"
                        />
                      ) : (
                        <img
                          src={Finish2}
                          className=""
                          alt="Finishing trophy"
                        />
                      )}
                    </Link>
                  </div>
                )}
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      {session.number === 10 ? (
        <Modal show={show} onHide={() => setShow(false)}>
          <Modal.Header closeButton>
            <Modal.Title>
              Want more computer science learning online?!
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <iframe
              src="https://docs.google.com/forms/d/e/1FAIpQLSdQIZTaTA8p-0Lzpqxh5MEAQNoyexHDE2aMs5bxZ-vl-dmVAQ/viewform?embedded=true"
              width="375"
              height="300"
              frameborder="0"
              marginHeight="0"
              marginWidth="0"
              title="Keep in touch"
            />
            <p>* Get permission from a Parent/Guardian before submitting</p>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShow(false)}>
              I've already signed up
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
    </>
  );
};

export default Session;
