import React, { useState } from "react";
import { Form, Button, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "../coding-summer-styles.css";

const ContactMe = () => {
  const [formContent, setFormContent] = useState({
    name: "",
    school: "",
    body: "",
  });
  const [displayMessage, setDisplayMessage] = useState(false);

  const handleChange = (event) => {
    setFormContent({ ...formContent, [event.target.name]: event.target.value });
  };

  const handleGenerateMessage = () => {
    setDisplayMessage(true);
  };

  return (
    <Card className="m-4 p-5">
      <div className={displayMessage ? "d-none" : ""}>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Name and Initial</Form.Label>
            <Form.Control
              name="name"
              placeholder="e.g. Alex M"
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="formBasicSchool">
            <Form.Label>Your School</Form.Label>
            <Form.Control
              name="school"
              placeholder="e.g. Agora Andorra"
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="formBasicBody">
            <Form.Label>Message</Form.Label>
            <Form.Control
              name="body"
              placeholder="e.g. Here's my work from today!"
              onChange={handleChange}
              as="textarea"
              rows="3"
            />
          </Form.Group>
          <Button
            className="session-button dblue-colour-bg no-border"
            onClick={handleGenerateMessage}
          >
            Generate Message
          </Button>
        </Form>
      </div>
      <div className={displayMessage ? "" : "d-none"}>
        <Card className="p-5 card-align-left">
          <p>
            <span className="font-weight-bold">To:</span>
            {`  david.ashaolu@agorainternationalandorra.com`}
          </p>
          <p>
            <span className="font-weight-bold">From:</span>
            {formContent.name}
          </p>
          <p>
            <span className="font-weight-bold">School:</span>
            {formContent.school}
          </p>
          <p>
            <span className="font-weight-bold">Message:</span>
            {formContent.body}
          </p>
        </Card>
        <div className="text-center">
          <p className="red-colour mt-4">
            * Don't forget to attach screenshots of your work & certificates!
          </p>
          <span>
            <Button
              className="session-button ge-blue-colour-bg text-white no-border"
              onClick={() => window.location.reload()}
            >
              Reset
            </Button>
          </span>
          <div className="session-button ge-blue-colour-bg text-white no-border">
            <a
              href={`mailto:david.ashaolu@agorainternationalandorra.com?subject=Cool Coding 2020&body=${`From: ${formContent.name} // School : ${formContent.school} //  Message : ${formContent.body}`}`}
            >
              Send
              <FontAwesomeIcon icon="envelope" className="ml-3 text-white" />
            </a>
          </div>
        </div>
      </div>
    </Card>
  );
};

export default ContactMe;
