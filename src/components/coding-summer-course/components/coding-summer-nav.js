import React from "react";
import { NavLink } from "react-router-dom";
// REAL / TEST CONTENT:
import { RoutesLinks } from "../../../app/components/routes-links";
// import { TestRoutesLinks } from "../../../app/components/test-route-links";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import GlobeducateLogo from "../../../images/globeducate-logo.png";
import "../coding-summer-styles.css";

const CodingSummerNav = ({ age_group, home }) => {
  return (
    <div className="summer-coding-nav red-colour-bg">
      <NavLink
        // REAL / TEST CONTENT:
        to={`/${RoutesLinks.CODINGSUMMER}/${home ? "" : age_group}`}
        // to={`/${TestRoutesLinks.CODINGSUMMER}/${home ? "" : age_group}`}
        className="back-link dblue-colour"
      >
        <FontAwesomeIcon
          icon="arrow-alt-circle-left"
          className="margin-left-20"
        />
      </NavLink>
      <span>|</span>
      <span>
        Cool Coding -
        {` ${
          age_group === "age_8-10"
            ? "8-10 years old"
            : age_group === "age_11-14"
            ? "11-14 years old"
            : ""
        }`}
      </span>
      <span>|</span>
      <img
        src={GlobeducateLogo}
        className="globeducate-logo-nav"
        alt="Globeducate logo"
      />
    </div>
  );
};

export default CodingSummerNav;
