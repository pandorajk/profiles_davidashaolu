import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// REAL / TEST CONTENT:
import SummerData from "../summer-course-data";
// import TestData from "../test-course-data";
import "../coding-summer-styles.css";

const SendFeedback = () => {
  const [display, setDisplay] = useState({
    d1: true,
    d2: false,
  });

  // REAL / TEST CONTENT:
  const { FINAL_CERTIFICATE } = SummerData;
  // const { FINAL_CERTIFICATE } = TestData;

  return (
    <div className="text-center m-4">
      <div className={display.d1 ? "" : "d-none"}>
        <a
          className="session-button ge-green-colour-bg no-border"
          href="https://forms.gle/uCeESrXPDnoYsgtY8"
          target="blank"
          onClick={() => setDisplay({ ...display, d1: false, d2: true })}
        >
          Send your feedback!
          <FontAwesomeIcon icon="external-link-square-alt" />
        </a>
      </div>
      <div className={display.d2 ? "" : "d-none"}>
        <a
          className="session-button ge-red-colour-bg no-border"
          href={FINAL_CERTIFICATE}
          target="blank"
          onClick={() => {
            setDisplay({ ...display, d3: true });
          }}
        >
          Get certificate!
          <FontAwesomeIcon icon="external-link-square-alt" />
        </a>
      </div>
      <div className={display.d3 ? "" : "d-none"}>
        <iframe
          src="https://giphy.com/embed/vmon3eAOp1WfK"
          width="480"
          height="357"
          frameBorder="0"
          class="giphy-embed"
          allowFullScreen
          title="celebration gif"
        ></iframe>
        <p>
          <a href="https://giphy.com/gifs/celebration-excited-loki-vmon3eAOp1WfK">
            via GIPHY
          </a>
        </p>
      </div>
    </div>
  );
};

export default SendFeedback;
