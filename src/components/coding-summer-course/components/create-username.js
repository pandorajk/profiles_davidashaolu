import React, { useState } from "react";
import { Col, Button, Form } from "react-bootstrap";
// REAL / TEST CONTENT:
import SummerData from "../summer-course-data";
// import TestData from "../test-course-data";
import "../coding-summer-styles.css";

const CreateUsername = (age_group) => {
  const [display, setDisplay] = useState({
    d1: true,
    d2: false,
    d3: false,
    d4: false,
    d5: false,
  });

  const [form, setForm] = useState({
    firstname: "",
    initial: "",
    school: "",
  });

  // REAL / TEST CONTENT:
  const { SCHOOLS } = SummerData;
  // const { SCHOOLS } = TestData;

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const schoolOptions = SCHOOLS.map((school) => (
    <option key={school.code} value={school.code}>
      {school.name}
    </option>
  ));

  return (
    <div className="text-center m-4">
      <div className={display.d1 ? "" : "d-none"}>
        <Button
          className="session-button ge-red-colour-bg no-border"
          onClick={() => setDisplay({ ...display, d1: false, d2: true })}
        >
          Get started!
        </Button>
      </div>
      <div className={display.d2 ? "" : "d-none"}>
        <Form>
          <Form.Row className="text-center">
            <Form.Group as={Col} className="text-center">
              <Form.Label>Name</Form.Label>
              <Form.Control
                name="firstname"
                placeholder="E.g. Alex"
                onChange={handleChange}
              />
              <Button
                className="session-button ge-red-colour-bg no-border"
                disabled={form.firstname === ""}
                onClick={() => setDisplay({ ...display, d2: false, d3: true })}
              >
                Next
              </Button>
            </Form.Group>
          </Form.Row>
        </Form>
      </div>
      <div className={display.d3 ? "" : "d-none"}>
        <Form>
          <Form.Row className="text-center">
            <Form.Group as={Col} controlId="initial">
              <Form.Label>Your Surname initial(s)</Form.Label>
              <Form.Control
                name="initial"
                placeholder="E.g. A"
                maxLength={2}
                onChange={handleChange}
              />
              <Button
                className="session-button ge-red-colour-bg no-border"
                disabled={form.initial === ""}
                onClick={() => setDisplay({ ...display, d3: false, d4: true })}
              >
                Next
              </Button>
            </Form.Group>
          </Form.Row>
        </Form>
      </div>
      <div className={display.d4 ? "" : "d-none"}>
        <Form>
          <Form.Row className="text-center">
            <Form.Group as={Col} controlId="school">
              <Form.Label>Your School</Form.Label>
              <Form.Control as="select" name="school" onChange={handleChange}>
                {schoolOptions}
              </Form.Control>
              <Button
                className="session-button ge-red-colour-bg no-border"
                disabled={form.school === ""}
                onClick={() => setDisplay({ ...display, d4: false, d5: true })}
              >
                Next
              </Button>
            </Form.Group>
          </Form.Row>
        </Form>
      </div>
      <div className={display.d5 ? "" : "d-none"}>
        <h3 className="font-weight-bold ge-green-colour">
          <p>Success!</p>
          <p>Here is your username:</p>
        </h3>
        <h1 className="font-weight-bold">{`${form.school}-${form.firstname}-${
          form.initial
        }${Math.floor(Math.random() * 99)}`}</h1>
        <p className={`red-colour ${age_group === "age_8-10" ? "" : "d-none"}`}>
          {age_group === "age_8-10"
            ? "Keep it safe and use it for Tynker and CodeSpark."
            : ""}
        </p>
      </div>
    </div>
  );
};

export default CreateUsername;
