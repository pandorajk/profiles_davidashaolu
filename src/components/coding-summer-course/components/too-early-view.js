import React from "react";
import TooEarlyImg from "../../../images/too-early.png";
import "../coding-summer-styles.css";

const TooEarly = () => {
  return (
    <div className=" splash-container-bottom mt-4 all-content-center">
      <img
        src={TooEarlyImg}
        className="too-early-avatar"
        alt="avatar saying too ealry"
      />
      <div className="h4 mt-4">
        <p>Whoops...you've come a bit too early for this session</p>
        <p>
          ... you will be able to view it soon when it's time to complete this
          session.
        </p>
      </div>
    </div>
  );
};

export default TooEarly;
