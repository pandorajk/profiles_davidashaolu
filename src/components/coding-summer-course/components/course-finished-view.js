import React from "react";
import { Button } from "react-bootstrap";
import Finish2 from "../../../images/finish2.jpeg";
import "../coding-summer-styles.css";

const CourseFinished = () => {
  return (
    <div className=" splash-container-bottom mt-4 all-content-center">
      <img
        src={Finish2}
        className="too-early-avatar"
        alt="avatar saying too ealry"
      />
      <div className="h4 mt-4">
        <p>Cool Coding 2020 has now finished !!!</p>
        <p>
          Check out the incredible showcase of work to see all the amazing work
          that has been created.
        </p>
        <Button className="session-button ge-green-colour-bg text-white no-border">
          <a href={"/portfolio/cool-coding-2020"}>{"Go to the Showcase!"}</a>
        </Button>
      </div>
    </div>
  );
};

export default CourseFinished;
