import React, { useState } from "react";
import { Col, Button, Form } from "react-bootstrap";
// REAL / TEST CONTENT:
import SummerData from "../summer-course-data";
// import TestData from "../test-course-data";
import "../coding-summer-styles.css";

const CSClassroom = () => {
  const [display, setDisplay] = useState({
    d1: true,
    d2: false,
    d3: false,
  });

  const [classroomCode, setClassroomCode] = useState("");

  // REAL / TEST CONTENT:
  const { SCHOOLS, INITIALOPTIONS } = SummerData;
  // const { SCHOOLS, INITIALOPTIONS } = TestData;

  const schoolOptions = SCHOOLS.map((school) => {
    if (school.csClassroom) {
      return (
        <option key={school.code} value={school.csClassroom}>
          {school.name}
        </option>
      );
    } else {
      return null;
    }
  });

  const initialOptions = INITIALOPTIONS.map((initial) => (
    <option key={initial.name} value={initial.csClassroom}>
      {initial.name}
    </option>
  ));

  return (
    <div className="text-center m-4">
      <div className={display.d1 ? "" : "d-none"}>
        <Form>
          <Form.Row className="text-center">
            <Form.Group as={Col} controlId="school">
              <Form.Label>Select your School</Form.Label>
              <Form.Control
                as="select"
                name="school"
                onChange={(e) => setClassroomCode(e.target.value)}
              >
                {schoolOptions}
              </Form.Control>
              <Button
                className="session-button ge-red-colour-bg no-border"
                disabled={classroomCode === "00" || !classroomCode}
                onClick={() => {
                  if (classroomCode === "I") {
                    setDisplay({ ...display, d1: false, d2: true });
                    setClassroomCode(
                      Object.values(INITIALOPTIONS)[0].csClassroom
                    );
                  } else {
                    setDisplay({ ...display, d1: false, d3: true });
                  }
                }}
              >
                Next
              </Button>
            </Form.Group>
          </Form.Row>
        </Form>
      </div>
      <div className={display.d2 ? "" : "d-none"}>
        <Form>
          <Form.Row className="text-center">
            <Form.Group as={Col} controlId="school">
              <Form.Label>The first letter of my username:</Form.Label>
              <Form.Control
                as="select"
                name="school"
                onChange={(e) => {
                  setClassroomCode(e.target.value);
                  setDisplay({ ...display, d2: false, d3: true });
                }}
              >
                {initialOptions}
              </Form.Control>
            </Form.Group>
          </Form.Row>
        </Form>
        <Button
          className="session-button ge-red-colour-bg no-border"
          disabled={classroomCode === "00" || !classroomCode}
          onClick={() => setDisplay({ ...display, d2: false, d3: true })}
        >
          Next
        </Button>
      </div>
      <div className={display.d3 ? "" : "d-none"}>
        <p>1. Go to CodeSpark </p>
        <p>2. Click on the 'Schools' icon </p>
        <p>3. Click on 'Students'</p>
        <p>4. Click on "Use Classroom Code"</p>
        <p>5. Enter this class code:</p>
        <h1>{classroomCode}</h1>
        <p>6. Click the green tick</p>
        <p>7. Click the green play button</p>
        <p>8. Click on the username you created in Session.1</p>
      </div>
    </div>
  );
};

export default CSClassroom;
