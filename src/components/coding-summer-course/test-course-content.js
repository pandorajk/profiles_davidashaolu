import { TestRoutesLinks } from "../../app/components//test-route-links";

const TestCourseContent = {
  "age_8-10": {
    [TestRoutesLinks.SESSION1]: {
      active: true,
      number: 1,
      mission: "Getting Started",
      resources: ["Adult support to help with signing up", "Notepad and pen"],
      youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs",
      webinar: " https://youtube.com",
      instructions: [
        [
          { line: "ACTIVITY 1a", type: "title" },
          {
            line: "Create your Username",
          },
          { line: "", type: "createUsername" },
        ],
        [
          { line: "ACTIVITY 1b", type: "title" },
          {
            line: "Copy and paste your username from above and send it to me:",
          },
          { line: "", type: "send-username" },
        ],
        [
          { line: "ACTIVITY 1c", type: "title" },
          {
            line: "Sign-up to *******:",
          },
          {
            line: "1. Copy your username from above",
            type: "important",
          },
          {
            line: "2. Paste the username when you sign-up to *******",
            type: "important",
          },
          {
            line: "3. Write down this classroom code on your notepad: ",
          },
          {
            line: `123456`,
            type: "code",
          },
          {
            line: "Sign up!",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "ACTIVITY 2", type: "title" },
          { line: "Watch video:" },
          { line: "Video Title" },
          {
            line: "Watch it!",
            link: "https://youtube.com",
          },
        ],
        [
          { line: "ACTIVITY 3", type: "title" },
          {
            line: "Activity 3 description",
          },
          {
            line: "Try it!",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "Extension Challenge.", type: "title" },
          {
            line: "Feeling brave! Have a go at...the extension challenge",
          },
          {
            line: "Go",
            link: "https://www.google.co.uk",
          },
        ],
      ],
    },
    [TestRoutesLinks.SESSION2]: {
      active: true,
      number: 2,
      mission: "Session 2 mission title",
      resources: ["Notepad and pen"],
      youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs",
      webinar: "https://youtube.com",
      instructions: [
        [
          { line: "ACTIVITY 1", type: "title" },
          {
            line: "Description for the 1st activity of session 2....",
          },

          {
            line: "Mission accepted",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "Extension Challenge", type: "title" },
          {
            line: "Extension challenge description.",
          },
          {
            line: "Try it!",
            link: "https://www.google.co.uk",
          },
        ],
      ],
    },
    [TestRoutesLinks.SESSION3]: {
      active: true,
      number: 3,
      mission: "Session 3 mission title",
      resources: ["Notepad and pen"],
      youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs",
      webinar: "https://youtube.com",
      instructions: [
        [
          { line: "ACTIVITY 1.", type: "title" },
          {
            line: "Signup for your CodeSpark classroom",
          },
          {
            line: "",
            type: "selectCodeSparkClassroom",
          },
          {
            line: "Sign-up",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "ACTIVITY 2.", type: "title" },
          { line: "Activity 2 description." },
          {
            line: "Go!",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "Extension Challenge.", type: "title" },
          { line: "Feeling brave? Do the extension challenge" },
          { line: "Try it!", link: "" },
        ],
      ],
    },
  },
  "age_11-14": {
    [TestRoutesLinks.SESSION1]: {
      active: true,
      number: 1,
      mission: "Getting Started and the History of Gaming!",
      resources: [
        "Adult support with signing up",
        "Notepad and pen",
        "Google Chrome is recommended for Activity 3.",
      ],
      youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs",
      webinar: " https://youtube.com",
      instructions: [
        [
          { line: "ACTIVITY 1a", type: "title" },
          {
            line: "Create a username",
          },
          { line: "", type: "createUsername" },
        ],
        [
          { line: "ACTIVITY 1b", type: "title" },
          {
            line: "Copy and paste your username from above and send it to me:",
          },
          { line: "", type: "send-username" },
        ],
        [
          { line: "ACTIVITY 2", type: "title" },
          { line: "Sign-up to ***:" },
          { line: "Click the link for a free account" },
          {
            line: "Sign up!",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "ACTIVITY 3", type: "title" },
          { line: "Watch video;" },
          { line: "'Video title'" },
          {
            line: "Watch",
            link: "https://www.youtube.com",
          },
        ],
        [
          { line: "ACTIVITY 4", type: "title" },
          {
            line: "Learn about the history of gaming with ***",
          },
          {
            line: "Learn",
            link: "https://www.google.co.uk",
          },
        ],
        [
          { line: "Extension Challenge.", type: "title" },
          { line: "Extension challenge description" },
          {
            line: "Start",
            link: "https://www.google.co.uk",
          },
        ],
      ],
    },
    [TestRoutesLinks.SESSION2]: {
      active: true,
      number: 2,
      mission: "Coding Fundamentals - Basic Syntax",
      resources: ["Your Username and Password", "Notepad and pen"],
      youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs",
      webinar: " https://youtube.com",
      instructions: [
        [
          { line: "ACTIVITY 1", type: "title" },
          {
            line: "Learn basic syntax ...",
          },
          {
            line: "Go!",
            link: "https://www.google.co.uk",
          },
        ],
        [
          {
            line: "Extension challenge: ",
            type: "title",
          },
          { line: "Extension challeng description" },
          {
            line: "Try it!",
            link: "https://www.google.co.uk",
          },
        ],
      ],
    },
    [TestRoutesLinks.SESSION3]: {
      active: true,
      number: 3,
      mission: "Loopy Loops",
      resources: ["Your Username and Password", "Notepad and pen"],
      youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs",
      webinar: " https://youtube.com",
      instructions: [
        [
          { line: "ACTIVITY 1", type: "title" },
          {
            line: "Debug your code and...",
          },
          {
            line: "Start",
            link: "https://www.google.co.uk",
          },
        ],
        [
          {
            line: "Extension challenge: ",
            type: "title",
          },
          { line: "Extension challenge description..." },
          {
            line: "Go!",
            link: "https://www.google.co.uk",
          },
        ],
      ],
    },
  },
};

export default TestCourseContent;
