import React from "react";
import { Link } from "react-router-dom";
// REAL / TEST CONTENT:
import { RoutesLinks } from "../../app/components/routes-links";
// import { TestRoutesLinks } from "../../app/components/test-route-links";

import Choose from "../../images/choose.png";
import GlobeducateLogo from "../../images/globeducate-logo.png";
import "./coding-summer-styles.css";

const CodingSummerHome = () => {
  return (
    <>
      <div className="splash-container-bottom mt-4">
        <img
          src={GlobeducateLogo}
          className="globeducate-logo"
          alt="Globeducate logo"
        />
        <h1 className="welcome-title dblue-colour">
          COOL CODING - SUMMER 2020!
        </h1>
        <div className="d-flex justify-content-center ">
          <Link
            // REAL / TEST CONTENT:
            to={`/${RoutesLinks.CODINGSUMMER}/age_8-10`}
            // to={`/${TestRoutesLinks.CODINGSUMMER}/age_8-10`}
            className="age-group-box all-content-center ge-blue-bg"
          >
            8-10 years old
          </Link>
          <div className="age-group-box all-content-center">
            <img
              src={Choose}
              className="choose-avatar"
              alt="welcome banner message"
            />
          </div>

          <Link
            // REAL / TEST CONTENT:
            to={`/${RoutesLinks.CODINGSUMMER}/age_11-14`}
            // to={`/${TestRoutesLinks.CODINGSUMMER}/age_11-14`}
            className="age-group-box all-content-center ge-green-bg"
          >
            11-14 years old
          </Link>
        </div>
      </div>
    </>
  );
};

export default CodingSummerHome;
