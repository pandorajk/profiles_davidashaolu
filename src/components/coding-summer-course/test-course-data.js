import TynkerLogo from "../../images/logos/tynker.png";
import KodableLogo from "../../images/logos/kodable.png";
import MinecraftHOCLogo from "../../images/logos/minecraft-hoc.png";
import CodeOrgLogo from "../../images/logos/code-org.png";
import CodeSparkLogo from "../../images/logos/codespark.png";
import CodeCombatLogo from "../../images/logos/code-combat.png";
import PixelHackLogo from "../../images/logos/pixel-hack.png";
import RobloxLogo from "../../images/logos/roblox.png";
import HOCLogo from "../../images/logos/hoc.png";

const INTRO = {
  youtube: "https://www.youtube.com/embed/ZbZSe6N_BXs?autoplay=1",
  task:
    "Task description. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  mission:
    "Mission description. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  remember: ["Point 1", "Another point", "Yet another point to remember"],
  "age_8-10": [
    TynkerLogo,
    KodableLogo,
    MinecraftHOCLogo,
    CodeSparkLogo,
    CodeOrgLogo,
    HOCLogo,
  ],
  "age_11-14": [
    CodeCombatLogo,
    PixelHackLogo,
    RobloxLogo,
    MinecraftHOCLogo,
    CodeOrgLogo,
    HOCLogo,
  ],
};

const OUTRO = {
  outroVideo: "https://www.youtube.com/embed/ZbZSe6N_BXs",
};

const SCHOOLS = [
  { name: "Select school...", code: "00", csClassroom: "00" },
  { name: "School 1", code: "01", csClassroom: "example1" },
  { name: "School 2", code: "02", csClassroom: "example2" },
  { name: "School 3", code: "03", csClassroom: "I" },
];

const INITIALOPTIONS = [
  { name: "A-M", csClassroom: "example3" },
  { name: "N-Z", csClassroom: "example4" },
];

const PASSWORD = "passwordExample";

const FINAL_CERTIFICATE = "https://google.com";

const TestCourseData = {
  INTRO,
  OUTRO,
  SCHOOLS,
  INITIALOPTIONS,
  PASSWORD,
  FINAL_CERTIFICATE,
};

export default TestCourseData;
